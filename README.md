# ESP32C3_ST7735

#### 介绍
合宙ESP32C3开发板配套0.96寸屏幕的Micropython版驱动，加入了中文字库的支持

#### 文件说明
1. fontlib.py：引用 [MicroPython New FontLib](https://gitee.com/walkline/micropython-new-fontlib) 项目，用于读取字库文件，注释掉了测试方法
2. combined.bin：引用 [FontMaker](https://gitee.com/kerndev/FontMaker) 项目生成的字库文件，此文件为微软雅黑16号字
3. ST7735.py：基于[ST7735驱动](https://github.com/cheungbx/st7735-esp8266-micropython)修改，增加了中文字库的支持
4. main_example.py：使用示例

#### 使用说明

1.  适用于合宙ESP32C3+搭配销售的0.96寸显示屏
2.  MicroPython v1.19.1下测试通过