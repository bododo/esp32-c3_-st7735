from machine import SPI, Pin

from ST7735 import TFT, bitSwap

import time
import framebuf

USE_FRAME_BUFFER = True

if USE_FRAME_BUFFER == False:
    spi = SPI(1, baudrate=40000000, polarity=0, phase=0,sck=Pin(2), mosi=Pin(3))
    tft=TFT(spi,'/combined.bin',6,10,7)
    tft.init_7735(tft.REDTAB80x160)

    time.sleep(1)

    tft.fill(tft.BLACK)
    chars = 'Micropython在合宙ESP32-C3+ST7735上支持中文字库'
    tft.text((0,0),chars,TFT.CYAN,(1,1))
    time.sleep(1)

    tft.fill(tft.BLACK)
    chars = '支持缩放（渣效果）'
    tft.text((0,0),chars,TFT.CYAN,(2,2))
    time.sleep(1)

    tft.fill(tft.BLACK)
    chars = '支持非等宽字体'
    tft.text((0,0),chars,TFT.CYAN,(2,2))
    time.sleep(1)

    tft.fill(tft.BLACK)
    chars = '支持彩色'
    tft.text((0,0),chars,TFT.RED,(2,2))
    time.sleep(1)

    tft.fill(tft.BLACK)
else:
    spi = SPI(1, baudrate=40000000, polarity=0, phase=0,sck=Pin(2), mosi=Pin(3))
    tft=TFT(spi,'/combined.bin',6,10,7,160,80)
    tft.init_7735(tft.REDTAB80x160)
    tft.fill(tft.BLACK)
    time.sleep(1)

    chars = 'Micropython在合宙ESP32-C3+ST7735上支持中文字库'
    tft.text((0,0),chars, bitSwap(TFT.CYAN),(1,1))
    buffer = tft.buffer
    tft.image(0,0,160,80,buffer)
    time.sleep(1)

    tft.buffer.fill(0)
    chars = '支持缩放（渣效果）'
    tft.text((0,0),chars, bitSwap(TFT.CYAN),(2,2))
    buffer = tft.buffer
    tft.image(0,0,160,80,buffer)
    time.sleep(1)

    tft.buffer.fill(0)
    chars = '支持非等宽字体'
    tft.text((0,0),chars, bitSwap(TFT.CYAN),(2,2))
    buffer = tft.buffer
    tft.image(0,0,160,80,buffer)
    time.sleep(1)

    tft.buffer.fill(0)
    chars = '支持彩色'
    tft.text((0,0),chars, bitSwap(TFT.RED),(2,2))
    buffer = tft.buffer
    tft.image(0,0,160,80,buffer)
    time.sleep(1)

    tft.buffer.fill(0)
    buffer = tft.buffer
    tft.image(0,0,160,80,buffer)